package math.geom.line.core.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import math.geom.line.core.Line;
import math.geom.line.core.Point;
import math.geom.line.core.exceptions.CoincidentPointsException;
import math.geom.line.operations.IncidenceResult;
import math.geom.line.operations.IncidenceResultStatus;
import math.geom.line.operations.LineOperations;

public class Geom2DLineOperationsTest {

	private Point p1;
	private Point p2;
	private Point p3;
	private Point p4;
	private Point p5;
	private Point p6;
	private Point p7;
	private Point p8;
	private Point p9;
	private Point p10;
	private Point p11;
	private Point p12;
	private Point pNull;
	private Point incidentPoint;
	private Point incidentPoint2;
	private Point incidentPoint3;
	private Line line1;
	private Line line2;
	private Line line3;
	private Line line4;
	private Line line5;
	private Line line6;
	private Line line7;
	private Line line8;
	
	@Before
	public void setUp() throws Exception {
		//line1 y = x defined by(p1,p2)		
		p1 = new Point(1,1);
		p2 = new Point(-1,-1);
		//Center belongs to line1
		p3 = new Point(0,0);
		//p4 does not belongs to line1		
		p4 = new Point(-1,0);
		//undefined line.. coincident points
		p5 = new Point(1,1);
		//line2 (p6,p7) parallel to line1 (p1,p2)
		p6 = new Point(1,0);
		p7 = new Point(0,-1);
		//line3 (p8,p9) is not parallel to line1 (p1,p2) 
		//intersection point is (1.5,1.5)
		p8 = new Point(1,2);
		p9 = new Point(3,0);
		//line4 (p3,p10) intersect line3 at point (1,2) 
		p10 = new Point(1,2);
		//no gradient representable line5 (p10,p11)
		p11 = new Point(1,4);
		//coincident lines line1 (p1,p2) and line6 (p1,p12)
		p12 = new Point(2,2);
		//Point null in order to test error in Line creation
		pNull = null;
		// y = x
		line1 = new Line(p1,p2);
		// y = x -1.0
		line2 = new Line(p6,p7);
		// y = -x + 3.0
		line3 = new Line(p8,p9);
		// y = 2.0x
		line4 = new Line(p3,p10);
		// x = 1
		line5 = new Line(p10,p11);
		// y = x
		line6 = new Line(p1,p12);
		// x = 0 
		line7 = new Line(p7,p3);
		// y = 2 (perpendicular to line5)
		line8 = new Line(p10,p12);
		//incidentPoint of (line1,line3)
		incidentPoint = new Point(1.5f,1.5f);
		//incidentPoint of (line3,line4)
		incidentPoint2 = new Point(1f,2f);
		//incidentPoint of (line1,line5)
		incidentPoint3 = new Point(1f,1f);
	}

	@Test(expected = CoincidentPointsException.class) 
	public void testExceptionThrown() {
		@SuppressWarnings("unused")
		Line lineErrorPointsEqual = new Line(p1,p5);
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testExceptionThrownGradientUndefined() {
		@SuppressWarnings("unused")
		Line lineErrorPointNull = new Line(p2,pNull);
	}
	
	@Test
	public void testEqualsLine() {
		Assert.assertTrue(line1.equals(line6));
	}
	
	@Test
	public void testNotEqualsLine() {
		Assert.assertTrue(!line1.equals(line4));
	}
	
	@Test
	public void testPointBelongsToLineTwoPointsFormula() {
		Assert.assertTrue(LineOperations.pointBelongsToLineTwoPointsRepresentation(line1, p3));
	}
	
	@Test
	public void testPointNotBelongsToLineTwoPointsFormula() {
		Assert.assertFalse(LineOperations.pointBelongsToLineTwoPointsRepresentation(line1, p4));
	}
	
	@Test
	public void testPointBelongsToLineGradientRepresentation() {
		Assert.assertTrue(LineOperations.pointBelongsToLineGradientRepresentation(line1, p12));
	}
	
	@Test
	public void testPointNotBelongsToLineGradientRepresentation() {
		Assert.assertFalse(LineOperations.pointBelongsToLineGradientRepresentation(line1, p9));
	}
	
	
	@Test
	public void testParallel() {
		Assert.assertTrue(LineOperations.parallel(line1,line2));
	}
	
	@Test
	public void testNotParallel() {
		Assert.assertFalse(LineOperations.parallel(line1,line3));
	}
	
	@Test
	public void testIncidence() {
		Assert.assertTrue(LineOperations.incident(line1,line3));
	}
	
	@Test
	public void testIncidencePoint() {
		IncidenceResult incidentResult = LineOperations.incidentPoint(line1,line3);
		Assert.assertTrue(IncidenceResultStatus.ONE_RESULT.equals(incidentResult.getIncidenceResultStatus()));
		Assert.assertTrue(incidentResult.getIncidencePoint().equals(incidentPoint));
	}
	
	@Test
	public void testIncidencePoint2() {
		IncidenceResult incidentResult = LineOperations.incidentPoint(line3,line4);
		Assert.assertTrue(IncidenceResultStatus.ONE_RESULT.equals(incidentResult.getIncidenceResultStatus()));
		Assert.assertTrue(incidentResult.getIncidencePoint().equals(incidentPoint2));
	}
	
	@Test
	public void testIncidencePoint3() {
		IncidenceResult incidentResult = LineOperations.incidentPoint(line1,line5);
		Assert.assertTrue(IncidenceResultStatus.ONE_RESULT.equals(incidentResult.getIncidenceResultStatus()));
		Assert.assertTrue(incidentResult.getIncidencePoint().equals(incidentPoint3));
	}
	
	@Test
	public void testIncidencePointCoincidentLines() {
		IncidenceResult incidentResult = LineOperations.incidentPoint(line1,line6);
		Assert.assertTrue(IncidenceResultStatus.INFINITE_RESULT.equals(incidentResult.getIncidenceResultStatus()));
	}
	
	@Test
	public void testIncidencePointForBothNotGradientRepresentableLines() {
		IncidenceResult incidentResult = LineOperations.incidentPoint(line5,line7);
		Assert.assertTrue(IncidenceResultStatus.NO_RESULT.equals(incidentResult.getIncidenceResultStatus()));
	}
	
	@Test
	public void testNoIncidencePointForParallelLine() {
		IncidenceResult incidentResult = LineOperations.incidentPoint(line1,line2);
		Assert.assertTrue(IncidenceResultStatus.NO_RESULT.equals(incidentResult.getIncidenceResultStatus()));
	}
	
	@Test
	public void testPerpendicular() {
		Assert.assertTrue(LineOperations.perpendicular(line5,line8));
	}
	
	@Test
	public void testPerpendicular2() {
		Assert.assertTrue(LineOperations.perpendicular(line3,line1));
	}
	
	@Test
	public void testPerpendicularBothNotGradientRepresentableLines() {
		Assert.assertFalse(LineOperations.perpendicular(line5,line7));
	}
	
}	
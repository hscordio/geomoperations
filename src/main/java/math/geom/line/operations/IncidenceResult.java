package math.geom.line.operations;

import math.geom.line.core.Point;

public class IncidenceResult {

	private IncidenceResultStatus incidenceResultStatus;
	public Point incidencePoint;
	
	
	public IncidenceResult(IncidenceResultStatus incidenceResultStatus) {
		super();
		this.incidenceResultStatus = incidenceResultStatus;
	}
	
	public IncidenceResult(IncidenceResultStatus incidenceResultStatus, Point incidencePoint) {
		super();
		this.incidenceResultStatus = incidenceResultStatus;
		this.incidencePoint = incidencePoint;
	}

	public IncidenceResultStatus getIncidenceResultStatus() {
		return incidenceResultStatus;
	}

	public void setIncidenceResultStatus(IncidenceResultStatus incidenceResultStatus) {
		this.incidenceResultStatus = incidenceResultStatus;
	}

	public Point getIncidencePoint() {
		return incidencePoint;
	}

	public void setIncidencePoint(Point incidencePoint) {
		this.incidencePoint = incidencePoint;
	}

}

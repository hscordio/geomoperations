package math.geom.line.operations;

import math.geom.line.core.Line;
import math.geom.line.core.Point;

public class LineOperations {

	
	//Definition of a line by means of two points
	public static boolean pointBelongsToLineTwoPointsRepresentation(Line line, Point p) {
		Point p1 = line.getP1();
		Point p2 = line.getP2();
		if(p1.getX()==p2.getX())
			return p.getX()==p1.getX();
		if(p1.getY()==p2.getY()){
			return p.getY()==p1.getY();
		}
		return (p.getX()-p1.getX())/(p2.getX()-p1.getX()) ==
			   (p.getY()-p1.getY())/(p2.getY()-p1.getY());
	}
	
	//Definition of a line by means of gradient and y-intercept
	public static boolean pointBelongsToLineGradientRepresentation(Line line, Point p) {
		if(line.isGradientYInterceptRepresentable()){
			return p.getY() == line.getGradient() * p.getX() + line.getyIntercept();
		}
		return false;
	}
	
	//Condition of parallelism of two lines
	public static boolean parallel(Line line1,Line line2){
		if(line1.equals(line2)){
			return false;
		}
		boolean l1GradientRepresentable = line1.isGradientYInterceptRepresentable();
		boolean l2GradientRepresentable = line2.isGradientYInterceptRepresentable();
		if(!l1GradientRepresentable && !l2GradientRepresentable)
			return true;
		if(l1GradientRepresentable){
			return l2GradientRepresentable? line1.getGradient() == line2.getGradient():false;
		}
		return !l2GradientRepresentable;
	}
	
	//Condition of perpendicularity of two lines
	public static boolean perpendicular(Line line1,Line line2){
		if(line1.equals(line2)){
			return false;
		}
		boolean l1GradientRepresentable = line1.isGradientYInterceptRepresentable();
		boolean l2GradientRepresentable = line2.isGradientYInterceptRepresentable();
		if(!l1GradientRepresentable && !l2GradientRepresentable)
			return false;
		if(l1GradientRepresentable){
			return l2GradientRepresentable? line1.getGradient() * line2.getGradient() == -1 : 
								 line1.getGradient()==0;
		}
		return line2.getGradient()==0;
	}
	
	//Condition of incidence of two lines 
	public static boolean incident(Line line1,Line line2){
		return !parallel(line1, line2);
	}

	//Definition of the incidence point
	public static IncidenceResult incidentPoint(Line line1,Line line2){
		if(line1.equals(line2)){
			return new IncidenceResult(IncidenceResultStatus.INFINITE_RESULT);
		}
		if(parallel(line1, line2)){
			return new IncidenceResult(IncidenceResultStatus.NO_RESULT);
		}
		boolean l1GradientRepresentable = line1.isGradientYInterceptRepresentable();
		boolean l2GradientRepresentable = line2.isGradientYInterceptRepresentable();
		if(l1GradientRepresentable && l2GradientRepresentable){
			float x = (line2.getyIntercept() - line1.getyIntercept())/
					  (line1.getGradient()-line2.getGradient());
			float y = line1.getGradient()*x+line1.getyIntercept();
			return new IncidenceResult(IncidenceResultStatus.ONE_RESULT, new Point(x,y));	
		}
		if(l1GradientRepresentable && !l2GradientRepresentable){
			float x = line2.getP1().getX();
			float y = line1.getGradient()*x+line1.getyIntercept();
			return new IncidenceResult(IncidenceResultStatus.ONE_RESULT, new Point(x,y));
		}
		if(!l1GradientRepresentable && l2GradientRepresentable){
			float x = line1.getP1().getX();
			float y = line2.getGradient()*x+line2.getyIntercept();
			return new IncidenceResult(IncidenceResultStatus.ONE_RESULT, new Point(x,y));
		}		
		return new IncidenceResult(IncidenceResultStatus.NO_RESULT);
	}
	
}
package math.geom.line.core;

import math.geom.line.core.exceptions.CoincidentPointsException;

public class Line {

	private Point p1; 
	private Point p2;
	
	private float gradient;
	private float yIntercept;
	
	public Line(Point p1, Point p2){
		if(p1==null || p2==null){
			throw new IllegalArgumentException("Points must be not null.");
		}
		if(p1.equals(p2)){
			throw new CoincidentPointsException("Line must be defined by means no-coincident points.");
		}
		this.p1=p1;
		this.p2=p2;
		if(isGradientYInterceptRepresentable()){
			gradient = calculateGradient();
			yIntercept = calculateYIntercept();
		}
	}
	
	public boolean isGradientYInterceptRepresentable() {
		return p1.getX()!=p2.getX();
	}
	
	private float calculateYIntercept() {
		return p1.getY() - p1.getX()*gradient;
	}

	private float calculateGradient() {
	    return (p2.getY()-p1.getY()) / (p2.getX()-p1.getX());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Line other = (Line) obj;
		boolean otherIsGradientRepresentable = other.isGradientYInterceptRepresentable();
		boolean thisIsGradientRepresentable = isGradientYInterceptRepresentable();
		if(!thisIsGradientRepresentable && !otherIsGradientRepresentable){
			return this.getP1().getX() == other.getP1().getX();
		}
		if(other.isGradientYInterceptRepresentable() && isGradientYInterceptRepresentable()){
			if (gradient != other.gradient)
				return false;
			if (yIntercept != other.yIntercept)
				return false;
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		if(isGradientYInterceptRepresentable()){
			return "y = "+getGradientString()+getYInterceptString();
		}
		if(p1.getX()==p2.getX())
			return " x = "+ p1.getX();
		if(p1.getY()==p2.getY()){
			return " y = "+ p1.getY();
		}
		String output = new String();
		output+=("   x "+getInverted(p1.getX())+"             y "+getInverted(p1.getY())+"\n");
		output+=(" --------------    =    --------------\n");
		output+=("  "+p2.getX()+" "+getInverted(p1.getX())+"           "+p2.getY()+" "+getInverted(p1.getY())+"\n");
		return output;
	}
	
	private String getYInterceptString() {
		return yIntercept==0 ? "": yIntercept>0 ? " + "+yIntercept : " "+yIntercept;
	}

	private String getGradientString() {
		return (gradient==0)?"": ((gradient==1)?"":(gradient==-1)?"-":gradient)+"x";
	}

	protected String getInverted(float x){
		return x>=0? " -  "+x:" + "+Math.abs(x);
	}
	
	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}
	
	public float getGradient() {
		return gradient;
	}

	public void setGradient(float gradient) {
		this.gradient = gradient;
	}

	public float getyIntercept() {
		return yIntercept;
	}

	public void setyIntercept(int yIntercept) {
		this.yIntercept = yIntercept;
	}
	
}

package math.geom.line.core.exceptions;

public class CoincidentPointsException extends RuntimeException{
	
	
	private static final long serialVersionUID = 7472268523228769648L;

	public CoincidentPointsException() {
	        super();
	    }

	 public CoincidentPointsException(String message) {
	        super(message);
	    }
	 
}
